<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Profile\AvatarController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketRepliesController;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Ticket;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // $user = DB::statement("select * from users");
    // $user = DB::select("select * from users");
    // $user = DB::select("select * from users where id=1");
    // $user = DB::select("select * from users where email=?",['ambalavanan@essopl.in']);
    // $user = DB::insert("insert into users (name, email, password) values (?,?,?)" ,['moorthi','ambalavanantesting@essopl.in','testing']);
    // $user = DB::insert("update users set email='ambalavanantest@essopl.in' where id=3");
    // $user = DB::insert("delete from users where id=3");
    // $user = DB::table('users')->get();
    // $user = DB::table('users')->first();
    // $user = DB::table('users')->where('id',3)->first();
    // $user = DB::table('users')->insert([
    //     'name' => 'testuser',
    //     'email' => 'testuser@test.com',
    //     'password' => 'testing'
    // ]);
    // $user = DB::table('users')->where('id',3)->update([
    //     'name' => 'testuser1',
    //     'email' => 'testuser1@test.com',
    // ]);
    // $user = DB::table('users')->where('id',3)->delete();
    // $user = User::get();
    // $user = User::create([
    //         'name' => 'testuser2',
    //         'email' => 'testuser2@test.com',
    //         'password' => 'testing'
    // ]);
    // $user = User::where('id',2)->update([
    //     'name' => 'testuser3',
    //     'email' => 'testuser3@test.com',
    // ]);
    // $user = User::find(3);
    // $user->update([
    //     'email' => 'testuser4@test.com',
    // ]);
    // $user = User::all();

    // $user = User::create([
    //             'name' => 'testuser5',
    //             'email' => 'testuse5@test.com',
    //             'password' => bcrypt('testing')
    //     ]);

    // $user = User::find(3);
    //     // dd($user);
    // dd($user->name);

    return view('welcome');
});

Route::get('/dashboard', function () {
    $user    = auth()->user();
    $open = $user->isAdmin ? Ticket::where('status','open')->count() : $user->tickets->where('status','open')->count();
    $resolved = $user->isAdmin ? Ticket::where('status','resolved')->count() : $user->tickets->where('status','resolved')->count();
    $approved = $user->isAdmin ? Ticket::where('status','approved')->count() : $user->tickets->where('status','approved')->count();
    $rejected = $user->isAdmin ? Ticket::where('status','rejected')->count() : $user->tickets->where('status','rejected')->count();
    $data = [
        'open'     => $open,
        'resolved' => $resolved,
        'approved' => $approved,
        'rejected' => $rejected,
    ];
    return view('dashboard',compact('data'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::patch('/profile/avatar', [AvatarController::class, 'update'])->name('avatar.profile.update');
    Route::post('/profile/openai', [AvatarController::class, 'generate'])->name('avatar.profile.generate');
    
});

Route::post('/auth/redirect', function () {
    return Socialite::driver('github')->redirect();
})->name('login.github');
 
Route::get('/auth/callback', function () {
    $user = Socialite::driver('github')->stateless()->user();
    //dd($user);
    $user = User::updateOrCreate(['email' => $user->email], [
        'name' => isset($user->name)?$user->name:$user->nickname,
        'password' => '12345678'
    ]);
    //dd($user);
    Auth::login($user);
    return redirect('/dashboard');
});

Route::middleware('auth')->group(function () {
    Route::resource('/ticket',TicketController::class);
});

Route::middleware('auth')->group(function () {
    Route::resource('/ticket_replay',TicketRepliesController::class);
});



require __DIR__.'/auth.php';
