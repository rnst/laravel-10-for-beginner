<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">
            User Avatar
        </h2>

        <p class="mt-1 text-sm text-gray-600">
           Add or Edit Avatar
        </p>
    </header>

    <style>
    .avatar {
    vertical-align: middle;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    }
    </style>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('avatar.profile.generate') }}" class="mt-6 space-y-6" enctype="multipart/form-data">
        @csrf
        <p class="mt-1 text-sm text-gray-600">Generate Profile - Avatar </p>

        <x-primary-button>Generate Avatar</x-primary-button>
    </form>

    <form method="post" action="{{ route('avatar.profile.update') }}" class="mt-6 space-y-6" enctype="multipart/form-data">
        @csrf
        @method('patch')

        @if (session('message'))
        <div style="color:red">
        {{ session('message') }}
        </div>
        @endif

        <img class="avatar" src='{{"/storage/$user->avatar"}}' alt="user avatar">

        <div>
            <x-input-label for="name" value="Avatar" />
            <x-text-input id="avatar" name="avatar" type="file" class="mt-1 block w-full" :value="old('avatar', $user->avatar)"  />
            <x-input-error class="mt-2" :messages="$errors->get('avatar')" />
        </div>

       
        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>

            @if (session('status') === 'profile-updated')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600"
                >{{ __('Saved.') }}</p>
            @endif
        </div>
    </form>
</section>
