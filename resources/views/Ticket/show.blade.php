<x-app-layout>
<div class="row">
<div class="col-md-6 col-xl-6">
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 dark:bg-gray-900">
        <h1 class="text-white text-lg font-bold">{{ $ticket->title }}</h1>
        <div class="w-full sm:max-w-xl mt-6 px-6 py-4 bg-white dark:bg-gray-800 shadow-md overflow-hidden sm:rounded-lg">
            <div class=" flex justify-between py-4">
                <p>{{ $ticket->description }}</p>
                <p>{{ $ticket->created_at->diffForHumans() }}</p>
                @if ($ticket->attachment)
                    <a href="{{ '/storage/' . $ticket->attachment }}" target="_blank">Attachment</a>
                @endif
            </div>

            <div class="flex justify-between">
                <div class="flex">
                    <a href="{{ route('ticket.edit', $ticket->id) }}">
                        <x-primary-button>Edit</x-primary-button>
                    </a>

                    <form class="ml-2" action="{{ route('ticket.destroy', $ticket->id) }}" method="post">
                        @method('delete')
                        @csrf
                        <x-primary-button>Delete</x-primary-button>
                    </form>
                </div>
                @if (auth()->user()->isAdmin)
                    <div class="flex">
                        <form action="{{ route('ticket.update', $ticket->id) }}" method="post">
                            @csrf
                            @method('patch')
                            <input type="hidden" name="status" value="approved" />
                            <x-primary-button>Approve</x-primary-button>
                        </form>
                        <form action="{{ route('ticket.update', $ticket->id) }}" method="post">
                            @csrf
                            @method('patch')
                            <input type="hidden" name="status" value="resolved" />
                            <x-primary-button>Resolved</x-primary-button>
                        </form>
                        <form action="{{ route('ticket.update', $ticket->id) }}" method="post">
                            @csrf
                            @method('patch')
                            <input type="hidden" name="status" value="rejected" />
                            <x-primary-button class="ml-2">Reject</x-primary-button>
                        </form>
                    </div>
                @else
                    <p class="">Status: {{ $ticket->status }} </p>
                @endif
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-6 col-xl-6">

<!------ Include the above in your HEAD tag ---------->
					<div class="card" style="height:650px;">
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight">
								<div class="user_info">
									<span>Comments</span>
									<p>{{count($ticket_replay)}} Messages</p>
								</div>
							</div>
						</div>
						<div class="card-body msg_card_body">
                        @if(count($ticket_replay) == 0)
                        <p class="text-center">No message here</p>
                        @else
                        @foreach($ticket_replay as $data)

                        @if($data->user_id == auth()->id())
                            <div class="d-flex justify-content-end mb-4">
							<div class="msg_cotainer_send">
                            <p class="text-decoration-underline action_menu_btn" style="font-size:12px;cursor:pointer;">{{$data->user->name}}</p>
                            {{$data->description}}
							<span class="msg_time">{{ $data->created_at->diffForHumans() }}</span>
							</div>
                            </div>

                            <!-- <div class="action_menu">
								<ul>
									
                                    <form class="ml-2" action="{{ route('ticket_replay.destroy', $data->id) }}" method="post">
                                        @method('delete')
                                        @csrf
									<li><i class="fas fa-ban"></i><button>Delete</button></li>
                                    </form>
								</ul>
							</div> -->


                        @else
                            <div class="d-flex justify-content-start mb-4">
							<div class="msg_cotainer">
                            <p class="text-decoration-underline action_menu_btn" style="font-size:12px;">{{$data->user->name}}</p>
                            {{$data->description}}
							<span class="msg_time">{{ $data->created_at->diffForHumans() }}</span>
							</div>
							</div>

                        @endif	
                            @endforeach
						</div>
                        @endif
						<div class="card-footer">
                        <form method="POST" action="{{ route('ticket_replay.store') }}" enctype="multipart/form-data">
                        @csrf
							<div class="input-group">
                                <input type="hidden" name="ticket_id" value="{{$ticket->id}}">
                                <input type="hidden" name="edit_flag" value="0">
								<textarea name="description" class="form-control" placeholder="Type your message..." required></textarea>
								<div class="input-group-append">
									<span class="input-group-text send_btn"><button>Reply</button></span>
								</div>
							</div>
                            </form>
						</div>
					</div>
				
	
    </div>
    </div>
</x-app-layout>
<script>

$(document).ready(function(){
$('.action_menu_btn').click(function(){
	$('.action_menu').toggle();
});

$(".editBtn").click(function(){
  console.log(this);
});

	});


</script>