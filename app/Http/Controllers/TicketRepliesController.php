<?php

namespace App\Http\Controllers;

use App\Models\TicketReplies;
use App\Http\Requests\StoreTicketRepliesRequest;
use App\Http\Requests\UpdateTicketRepliesRequest;

class TicketRepliesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTicketRepliesRequest $request)
    {
       $ticket_replay = TicketReplies::create([
        'ticket_id'   => $request->ticket_id,
        'description' => $request->description,
        'user_id'     => auth()->id(),
        'edit_flag'   => $request->edit_flag,
        ]);

        return back();
        return redirect(route('ticket.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(TicketReplies $ticketReplies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(TicketReplies $ticketReplies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTicketRepliesRequest $request, TicketReplies $ticketReplies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(TicketReplies $ticketReplies)
    {
       // dd($ticketReplies);
        $ticketReplies->delete();
        return back();

    }
}
