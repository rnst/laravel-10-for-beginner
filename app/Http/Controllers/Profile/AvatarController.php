<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAvatarRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use OpenAI\Laravel\Facades\OpenAI;
use Illuminate\Support\str;


class AvatarController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAvatarRequest $request)
    {
        $path = Storage::disk('public')->put('avatar',$request->file('avatar'));
        // $path = $request->file('avatar')->store('avatar','public');
        if($oldAvatar = $request->user()->avatar){
            Storage::disk('public')->delete($oldAvatar);
        }
        auth()->user()->update(['avatar' => $path]);
        return back()->with('message', 'Avatar Updated!...');
    }

    public function generate(Request $request)
    {
        $result = OpenAI::images()->create([
            'prompt' => 'create avatar for user with cool style animated',
            'n'      => 1,
            'size'   => '256x256'
            ]);
        $content = file_get_contents($result->data[0]->url);
       // dd($content);
        $filename = str::random(25);
        //dd($filename);
        if($oldAvatar = $request->user()->avatar){
            Storage::disk('public')->delete($oldAvatar);
        }
        $path = Storage::disk('public')->put("avatar/$filename.jpg",$content);
        auth()->user()->update(['avatar' => "avatar/$filename.jpg"]);
        return redirect(route('profile.edit'))->with('message', 'Avatar Updated!...');
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
