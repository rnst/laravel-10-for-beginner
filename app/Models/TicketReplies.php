<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;

class TicketReplies extends Model
{
    use HasFactory;

    protected $fillable = [
        'ticket_id',
        'description',
        'user_id',
        'edit_flag'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
